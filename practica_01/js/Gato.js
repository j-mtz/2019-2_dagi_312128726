export default class Gato {
  constructor() {
    this.container = document.querySelector("#container");
    this.jugador_actual = "j1";
    this.turn = document.getElementById("turn");
    this.info = document.getElementById("info");
    this.info_winner = document.getElementById("info_winner");
    this.arreglo = new Array(9);
    // Hotfix para el arreglo
    // Inicializa el arreglo con valores diferentes para la verificación
    for(var i = 0; i < this.arreglo.length; i++) {
      this.arreglo[i] = i;
    }

    let self = this;
    this.container.addEventListener("mousedown", function(evt) {
      //console.log("this =", this, ", self =", self);
      console.log("Jugador actual =", self.jugador_actual);

      // Verifica que la casilla este vacia.
      if (!evt.target.getAttribute("clicked")) {
        if (evt.target != self.container) {
          // Guarda el id del hijo seleccionado
          var child_id = evt.target.id.substr(evt.target.id.length -1)
          // Guarda en el arreglo que jugador jugó y su posición
          self.arreglo[child_id-1] = self.jugador_actual;
          // Verifica el jugador y dibuja la casilla
          if(self.jugador_actual == "j1"){
            evt.target.setAttribute("clicked", "j1");
            self.jugador_actual = "j2";
            self.turn.innerText = "Turno del jugador 2";
          } else {
            evt.target.setAttribute("clicked", "j2");
            self.jugador_actual = "j1";
            self.turn.innerText = "Turno del jugador 1";
          }
          console.log("Array: ", self.arreglo);    
          
          check_Game(self.jugador_actual);
        }
      }
    });

    /**
     * Veriifica si el juego termina o se puede seguir jugando
     * @param {*} opposing_player 
     */
    function check_Game(opposing_player){
      // Verificaciones horizontales
      var check_1 = self.arreglo[0] == self.arreglo[3] && self.arreglo[3] == self.arreglo[6];
      var check_2 = self.arreglo[1] == self.arreglo[4] && self.arreglo[4] == self.arreglo[7];
      var check_3 = self.arreglo[2] == self.arreglo[5] && self.arreglo[5] == self.arreglo[8];
      // Verificaciones verticales
      var check_4 = self.arreglo[0] == self.arreglo[1] && self.arreglo[1] == self.arreglo[2];
      var check_5 = self.arreglo[3] == self.arreglo[4] && self.arreglo[4] == self.arreglo[5];
      var check_6 = self.arreglo[6] == self.arreglo[7] && self.arreglo[7] == self.arreglo[8];
      // Verificaciones diagonales
      var check_7 = self.arreglo[0] == self.arreglo[4] && self.arreglo[4] == self.arreglo[8];
      var check_8 = self.arreglo[2] == self.arreglo[4] && self.arreglo[4] == self.arreglo[6];

      var check_draw1 = self.arreglo[0] == "j1" || self.arreglo[0] == "j2";
      var check_draw2 = self.arreglo[1] == "j1" || self.arreglo[1] == "j2";
      var check_draw3 = self.arreglo[2] == "j1" || self.arreglo[2] == "j2";
      var check_draw4 = self.arreglo[3] == "j1" || self.arreglo[3] == "j2";
      var check_draw5 = self.arreglo[4] == "j1" || self.arreglo[4] == "j2";
      var check_draw6 = self.arreglo[5] == "j1" || self.arreglo[5] == "j2";
      var check_draw7 = self.arreglo[6] == "j1" || self.arreglo[6] == "j2";
      var check_draw8 = self.arreglo[7] == "j1" || self.arreglo[7] == "j2";
      var check_draw9 = self.arreglo[8] == "j1" || self.arreglo[8] == "j2"; 

      // Checa las diferentes posiciones del arreglo y sus coincidencias
      if(check_1 || check_2 || check_3 || check_4 || 
        check_5 || check_6 || check_7 || check_8) {
        if(opposing_player == "j1"){
          self.info_winner.innerText = "Gano el jugador 2";
        }else{
          self.info_winner.innerText = "Gano el jugador 1";
        }
        self.info.style.visibility = "visible";
      }
      if(check_draw1 && check_draw2 && check_draw3 && check_draw4 &&
        check_draw5 && check_draw6 && check_draw7 && check_draw8) {
        self.info_winner.innerText = "Empate";
        self.info.style.visibility = "visible";
      }
    }
  }
}