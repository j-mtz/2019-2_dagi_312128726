/**
 * Clase Ball del ejemplo ejemplo_128_multiples_pelotas_colisiones/
 */
export default class Ball {
  /**
   * Constructor del objeto Ball
   * @param {*} x 
   * @param {*} y 
   * @param {*} radius 
   * @param {*} color 
   */
  constructor(x = 0, y = 0, radius = 1, color = "#000000") {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.color = color;
  }

  /**
   * Método que actualiza la posicion del objeto Ball
   * @param {*} elapsed 
   * @param {*} x 
   * @param {*} y 
   */
  update(elapsed, x, y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Método que dibuja el objeto Ball
   * @param {*} context 
   */
  render(context) {
    context.beginPath();
    context.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
    context.fillStyle = this.color;
    context.fill();
  }
}