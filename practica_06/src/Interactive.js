import Ball from "./Ball.js";
import KEY from "./Key.js";
import Line from "./Line.js";
import Ship from "./Ship.js";

export default class Interactive {
  constructor(container) {
    // Elementos del contenedor y del canvas
    this.container = container;
    this.canvas = container.querySelector("canvas");
    this.context = this.canvas.getContext("2d");
    // Crea la nave y la pone a la mitad del canvas
    this.ship = new Ship(this.canvas.width / 2, 0, 40);
    this.ship_position = { x: this.canvas.width / 2, y: 0 };
    // Arreglo de segmentos
    // Contiene todas las lineas que representan al terreno
    this.arrLine = [];
    // Circulo Auxiliar
    // Se instancia y se mueve con la nave, utilizado para detectar colisiones
    // y así poder reutilizar código de ejemplos
    this.circuloAux = new Ball(this.ship_position.x, this.ship_position.y, this.ship.size / 2);

    // Booleano para saber si el juego sigue activo
    this.game = true;

    // Arreglo de puntos con las coordenadas de puntos en x y y de los 
    // cuales se generan los segmentos de recta
    this.arrPoint = [];
    // Distancia de cada segmento
    this.distanciaSeg = 55;
    // Coordenada x actual para que los segmentos esten unidos
    this.actualX = -10;
    for (let i = 0; i < 8; i++) {
      this.actualY = Math.floor(Math.random() * 200) + 400;
      this.arrPoint[i] = { x: this.actualX, y: this.actualY };
      this.actualX += this.distanciaSeg;
    }
    this.actualY = Math.floor(Math.random() * 200) + 400;
    // Representa el segmento plano para poder aterrizar
    this.partePlana = Math.floor(Math.random() * 6);
    this.arrPoint[this.partePlana].y = this.actualY;
    this.arrPoint[this.partePlana + 1].y = this.actualY;
    this.arrPoint[this.partePlana + 2].y = this.actualY;

    // Crea 7 segmentos de linea para el terreno
    // Crear mas no asegura tener terrenos planos donde aterrizae
    for (let i = 0; i < 7; i++) {
      this.arrLine[i] = new Line(this.arrPoint[i].x, this.arrPoint[i].y, this.arrPoint[i + 1].x, this.arrPoint[i + 1].y);
    }

    // EventListeners para las pulsaciones del teclado
    window.addEventListener("keydown", (evt) => {
      KEY.onKeyDown(evt.key);
    });
    window.addEventListener("keyup", (evt) => {
      KEY.onKeyUp(evt.key);
    });

    // Botones para reiniciar
    this.retro = document.querySelector("#retro");
    this.retro_txt = document.querySelector("#retro_txt");
    this.retro_btn = document.querySelector("#retro_btn");
    this.retro_btn.addEventListener("mousedown", () => this.restart());
  }

  /**
   * Método para procesar la entrada
   * Procesa la entrada y actualiza la nave
   */
  processInput() {
    this.ship.processInput(KEY);
  }

  /**
   *  Método que actualiza la logica del juego
   *  Mientras  el juego siga activo actualiza la posicion de la nave y 
   * revisa si el juego se ha ganado
   * @param {*} elapsed 
   */
  update(elapsed) {
    if(this.game) {
      this.circuloAux.update(elapsed, this.ship.x, this.ship.y);
      this.ship.update(elapsed);
      // Si aterriza termina el juego
      if(this.checkLanding()) {
        this.game = false;
        this.retro_txt.innerHTML = "¡Ganaste!";
        this.retro.style.display = 'block';
      // En otro caso revisa que no haya colisionado con algun segmento
      } else {
        this.arrLine.forEach((line) => {
          this.checkCollision(line, this.circuloAux);
        });    
      }
  
    }
  }

  /**
   *  Método que renderiza todo el juego
   *  Renderiza el juego y la nave, ademas del terreno
   * @param {*} elapsed 
   */
  render() {
    // Dibuja el canvas
    this.context.fillStyle = "black";
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
    // Dibuja los segmentos de recta
    this.arrLine.forEach((line) => {
      line.render(this.context);
    });
    // Dibuja la nave
    this.ship.render(this.context);
  }

  /**
   * Método que revisa si la nave ha aterrizado
   */
  checkLanding() {
    // Revisa que esté en la parte plana y la nave esté dentro de las coordenadas
    if (this.ship.x > this.partePlana * this.distanciaSeg && 
      this.ship.x < (this.partePlana + 2) * this.distanciaSeg && 
      (this.ship.y + this.ship.size / 2) > this.arrPoint[this.partePlana].y) {
      return true;
    }
  }

  /**
   * Método auxiliar para detectar colisiones
   * Detecta las colisiones entre un punto y el círculo
   * @param {*} circle 
   * @param {*} point 
   */
  checkCollisionAux(circle, point) {
    let aux1 = (point.x - circle.x);
    let aux2 = (point.y - circle.y);
    let distancia = Math.sqrt(aux1 * aux1 + aux2 * aux2);
    // Si la distancia entre los dos puntos es menor que el radio,
    // i.e. ya lo toco
    if(distancia < circle.radius) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Método que detecta las colisiones de la nave con algun lineo del terreno
   * @param {*} line 
   * @param {*} circle 
   */
  checkCollision(line, circle) {
    if (this.checkCollisionAux(circle, { x: line.x1, y: line.y1 }) ||
      this.checkCollisionAux(circle, { x: line.x2, y: line.y2 })) {
      this.game = false;
      this.retro_txt.innerHTML = "¡Perdiste! :(";
      this.retro.style.display = 'block';
    }
  }

  /**
   * Función para reiniciar el juego
   */
  restart() {
    window.location.reload();
  }
}