/**
 * Clase para dibujar una línea en el terreno aleatorio
 */
export default class Line {
  /**
   * Constructor de una linea
   * @param {*} x1 
   * @param {*} y1 
   * @param {*} x2 
   * @param {*} y2 
   * @param {*} color 
   */
  constructor(x1 = 0, y1 = 0, x2 = 10, y2 = 10) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;

    this.color = "white";
  }

  /**
   * Método que dibuja la línea
   * @param {*} context 
   */
  render(context) {
    context.beginPath();
    context.lineWidth = 3;
    context.strokeStyle = this.color;
    context.moveTo(this.x1, this.y1);
    context.lineTo(this.x2, this.y2);
    context.stroke();
  }
}
