const PI_180 = Math.PI / 180;
const drawing_angle = 125;
const ROTATE_ANGLE = 3;
const SHIP_FORCE = 20;
let gravity = 4;
let sin;
let cos;

/**
 * Clase para representar a la nave
 */
export default class Ship{
  constructor(x = 0, y = 0, size = 1) {

    this.x = x;
    this.y = y;
    this.w = size;
    this.h = size;
    this.size = size;
    this.rotation = 270 * PI_180;
    this.force = 0;
    this.friction = 0.95;
    this.velocity = { x: 0, y: 0 };
    this.acceleration = { x: 0, y: 0 };
    this.abajo_flecha_pressed = false;

    this.image = new Image();
    this.image.src = "img/space_ship.svg";
  }

  /**
   * Procesa la entrada de las flechas
   * @param {*} KEY 
   */
  processInput(KEY) {
    // Obtiene la imagen control.svg
    let svg = document.querySelector("#navigation");
    // Obtiene los elementos contenidos dentro de control.svg
    let flechas = svg.contentDocument;
    // Flechas para controlar cada cosa
    let izq_flecha = flechas.querySelector("#izq_flecha");
    let der_flecha = flechas.querySelector("#der_flecha");
    let abajo_flecha = flechas.querySelector("#abajo_flecha");

    abajo_flecha.addEventListener('mousedown', () => {
      this.abajo_flecha_pressed = true;
    });

    abajo_flecha.addEventListener('mouseup', () => {
      this.abajo_flecha_pressed = false;
    });
    // Click flecha izquierda
    izq_flecha.addEventListener('mousedown', () => {
      this.velocity.x=50;
    });
    // Click flecha derecha
    der_flecha.addEventListener('mousedown', () => {
      this.velocity.x=-50;
    });
    // Presiona flecha izquierda
    if (KEY.isPressed(KEY.LEFT)) {
      this.velocity.x=50;
    }
    // Presiona flecha derecha
    if (KEY.isPressed(KEY.RIGHT)) {
      this.velocity.x--;
    }
    if (KEY.isPressed(KEY.DOWN)) {
      this.force = SHIP_FORCE;
    }
    // Resetea la 'gravedad' cuando se deja de presionar la tecla
    if (!KEY.isPressed(KEY.DOWN) && !this.abajo_flecha_pressed) {
      this.force = 0;
    }
  }

  /**
   * Método para actualizar los parámetros de la nave
   * @param {*} elapsed 
   */
  update(elapsed) {
    this.acceleration.x = Math.cos(this.rotation) * this.force;
    this.acceleration.y = Math.sin(this.rotation) * this.force;

    this.velocity.x += this.acceleration.x;
    this.velocity.y += this.acceleration.y;
    // La friccion hace que vuelva a bajar
    this.velocity.x *= this.friction;
    this.velocity.y *= this.friction;

    this.x += this.velocity.x * elapsed;
    this.y += this.velocity.y * elapsed;
    this.y += gravity;
  }

  /**
   * Método para dibujar la nave
   * @param {*} context 
   */
  render(context) {
    cos = Math.cos(drawing_angle * PI_180);
    sin = Math.sin(drawing_angle * PI_180);

    context.save();
    context.translate(this.x, this.y);
    context.rotate(this.rotation + 90 * Math.PI / 180);
    context.drawImage(this.image, -this.w / 2, -this.h / 2, this.w, this.h);
    context.restore();
  }
}
