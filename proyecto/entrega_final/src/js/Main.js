import Matrix from "./Matrix.js";

// La mantenemos mas accesibles estas variables para poder compartirlas
var matrix = null;
var indexFila = null;
var indexColumna = null;
// Matrices de suma
var matrix3 = null;
var matrix4 = null;
// Multiplicacion por escalar
var escalar = null;
var matrix5 = null;
// Matrices de multiplicación
var matrix6 = null;
var matrix7 = null;
// Segundo ejercicio de cierre
var matrix8 = null;
var matrix9 = null;
// Tercer ejercicio de cierre
var matrix10 = null;
var matrix11 = null;
var randomCierre = null;

// Las operaciones son retiradas de la clase Matrix.js para no interferir
// con su valores y su memoria, además crean nuevos objetos

/**
 * Método para sumar una matriz con otra matriz recibidas como parámetro.
 * @param {*} otherMatrix La matriz a sumar con la actual.
 */
function sumMatrix(matrix01, matrix02){
    var m1 = matrix01.getMatrix()
    var m2 = matrix02.getMatrix();
    var size = m1.length;
    var m3 = new Array(size);
    for(var i = 0; i < size; i++) {
        m3[i] = new Array(size);
    }
    // Itera sobre la matriz y suma el valor de la siguiente
    for(var i = 0; i < size; i++) {
        for(var j = 0; j < size; j++){
            m3[i][j] = m1[i][j] + m2[i][j];
        }
    }
    return new Matrix(m3);
}

/**
 * FUnción para multiplicar una matriz por un escalar.
 * @param {*} matrix01 La matriz a multiplicar
 * @param {*} escalar El escalar
 */
function multEscalar(matrix01, escalar) {
    var m1 = matrix01.getMatrix()
    var size = m1.length;
    var m3 = new Array(size);
    for(var i = 0; i < size; i++) {
        m3[i] = new Array(size);
    }
    // Itera sobre la matriz y suma el valor de la siguiente
    for(var i = 0; i < size; i++) {
        for(var j = 0; j < size; j++){
            m3[i][j] = m1[i][j] * escalar;
        }
    }
        return new Matrix(m3);
}

/**
 * Método para multiplicar la matriz con otra matriz que recibe como parámetro.
 * @param {*} matrix01 Matriz01 a multiplicar.
 * @param {*} matrix02 Matriz01 a multiplicar.
 */
function multMatrix(matrix01, matrix02){
    var m1 = matrix01.getMatrix()
    var m2 = matrix02.getMatrix();
    var size = m1.length;
    var result = [];
    for(var i = 0; i < size; i++){
        result[i] = [];
        for(var j = 0; j < size; j++){
            var sum = 0;
            for(var k = 0; k < size; k++){
                sum += m1[i][k] * m2[k][j];
            }
            result[i][j] = sum;
        }
    }
    return new Matrix(result);
}

/**
 * Función para gener un número aleatorio dentro del rango dado
 * @param {*} min Rango mínimo
 * @param {*} max Rango máximo
 */
function randomNumber(min, max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}

/**
 * Función para saber si un número está en un rango dado
 * @param {*} x El número del que se quiere saber el rango
 * @param {*} min Rango mínimo
 * @param {*} max Rango máximo
 */
function inRange(x, min, max) {
    return ((x-min)*(x-max) <= 0);
}

/**
 * Función para colocar la matriz deseada en la sección Inicio 02
 * donde se muestran los tipos de matrices.
 * @param {*} matrixType 
 */
function putMatrixType(matrixType) {
    // Obtiene y limpia el div donde va la matriz
    var div_matriz = document.getElementById("div_matriz_inicio_02");
    div_matriz.innerHTML = "";
    var matrix = new Matrix(matrixType);
    // Dibuja la matriz
    drawMatrixAsTable(matrix, div_matriz, "matriz_inicio_02");
}

/**
 * Función para remplazar un elemento por otro, usado en la navegación del recurso
 * @param {*} div01 El elemento actual
 * @param {*} div02 El elemento por el que se va a remplazar
 */
function replaceElement(element01, element02) {
    var div1 = document.getElementById(element01);
    var div2 = document.getElementById(element02);
    div1.style.display = "none";
    div2.style.display = "block";
}

/**
 * Función para cambiar la matrices operandos de la suma dependiendo 
 * del paso en el que va
 * @param {*} numPaso 
 */
function changeRowOperator(numPaso, matrix01ID, matrix02ID) {
    var tabla01 = document.getElementById(matrix01ID);
    var rows01 = tabla01.getElementsByTagName("tr");
    var tabla02 = document.getElementById(matrix02ID);
    var rows02 = tabla02.getElementsByTagName("tr");
    switch(numPaso){
        case 1:
            rows01[0].className = rows02[0].className = "fila_resultado";
            rows01[1].className = rows02[1].className = "fila_operando";
            rows01[2].className = rows02[2].className = "fila_operando";
            break;
        case 2:
            rows01[0].className = rows02[0].className = "fila_operando";
            rows01[1].className = rows02[1].className = "fila_resultado";
            rows01[2].className = rows02[2].className = "fila_operando";
            break;
        case 3:
            rows01[0].className = rows02[0].className = "fila_operando";
            rows01[1].className = rows02[1].className = "fila_operando";
            rows01[2].className = rows02[2].className = "fila_resultado";
            break;
    }
}

/**
 * Función para cambiar el color de la matriz operante en la multiplicación
 * por escalar
 * @param {*} numPaso EL número de paso actual
 */
function changeTableOperatorEscalar(numPaso) {
    var table = document.getElementById("matriz_desarrollo_escalar_02");
    var actual = 1;
    for (let row of table.rows) {
        for(let cell of row.cells) {
            // Usar el paso actual para saber que parte dibujar
            if(actual < numPaso){
                cell.className = "fila_operando";
            } else if (actual === numPaso) {
                cell.className = "fila_resultado";
            } else {
                cell.className = "fila_operando";
            }
        actual++;
        }
    }
}

/**
 * Función para cambiar la matriz de resultado de la suma dependiendo 
 * del paso en el que va
 * @param {*} numPaso 
 */
function changeRowResult(numPaso, matrixID) {
    var tabla = document.getElementById(matrixID);
    var rows = tabla.getElementsByTagName("tr");
    switch(numPaso){
        case 1:
            rows[0].className = "fila_resultado";
            break;
        case 2:
            rows[0].className = "fila_normal"; 
            rows[1].className = "fila_resultado";
            break;
        case 3:
            rows[0].className = "fila_normal"; 
            rows[1].className = "fila_normal"; 
            rows[2].className = "fila_resultado";
            break;
    }
}

/**
 * Función para cambiar las tablas en la multiplicación por escalar
 * @param {*} numPaso Número de paso actual
 * @param {*} matrixID EL ID de la matriz
 */
function changeTableMultEscalar(numPaso, matrixID) {
    var table = document.getElementById(matrixID);
    var actual = 1;
    for (let row of table.rows) {
        for(let cell of row.cells) {
            // Usar el paso actual para saber que parte dibujar
            if(actual < numPaso){
                cell.className = "fila_normal";
            } else if (actual === numPaso) {
                cell.className = "fila_resultado";
            }
        actual++;
        }
    }
}

/**
 * Funcion para cambiar el texto de un div, usado en la sección Desarrollo
 * @param {*} divInfoID 
 * @param {*} text 
 */
function changeTextInfoDesarrollo(divInfoID, text) {
    var div = document.getElementById(divInfoID);
    div.innerHTML = "";
    var p = document.createElement("P");
    p.innerHTML = text;
    div.appendChild(p);
}

/**
 * Funcion para colocar los operandos usados en las operaciones
 * @param {*} numPaso El número de paso actual en el que se ejecuta
 */
function setOperandosSuma(numPaso) {
    var x = numPaso -1;

    var m1 = matrix3.getMatrix();
    var m2 = matrix4.getMatrix();
    // Número de matriz - Celda de la matriz
    var op_1_x0 = m1[x][0];
    var op_2_x0 = m2[x][0];
    var op_1_x1 = m1[x][1];
    var op_2_x1 = m2[x][1];
    var op_1_x2 = m1[x][2];
    var op_2_x2 = m2[x][2];
    var span = document.createElement("SPAN");
    var span_content = document.createTextNode("[(" + op_1_x0 + ") + (" + op_2_x0 + ")]  [(" + op_1_x1 + ") + (" + op_2_x1 + ")]  [(" + op_1_x2 + ") + (" + op_2_x2 + ")]");
    span.appendChild(span_content);
    span.className = "math";
    var div = document.getElementById("div_info_suma");
    // Agrega un salto de línea antes del span
    var br = document.createElement('br');
    div.appendChild(br);
    div.appendChild(span);
}

/**
 * Funcion para colocar los operandos usados en las multiplicaciones por escalar
 * @param {*} numPaso El número de paso actual en el que se ejecuta
 */
function setOperatorEscalar(numPaso) {
    var m = matrix5.getMatrix();
    var fila = null;
    var columna = numPaso % 3;
    if(columna === 0){
        columna = 2;
    } else {
        columna -= 1;
    }
    // Celda de matriz
    if(inRange(numPaso, 1, 3)){
        fila = 0
    }
    if(inRange(numPaso, 4, 6)){
        fila = 1;
    }
    if(inRange(numPaso, 7, 9)){
        fila = 2;
    }
    var value = m[fila][columna];
    var span = document.createElement("SPAN");
    var span_content = document.createTextNode("("+ value + ") * ("+ escalar + ")");
    span.appendChild(span_content);
    span.className = "math";
    var div = document.getElementById("div_info_escalar");
    // Agrega un salto de línea antes del span
    var br = document.createElement('br');
    div.appendChild(br);
    div.appendChild(span);
}

/**
 * Funcion para colocar los operandos usados en las multiplicaciones
 * No funciona correctamente
 * @param {*} numPaso El número de paso actual en el que se ejecuta
 */
function setOperatorMult(numPaso) {
    var m1 = matrix6.getMatrix();
    var m2 = matrix6.getMatrix();
    // Fila de m1
    var fila = null;
    // Columna de m2
    var columna = numPaso % 3;
    if(columna === 0){
        columna = 2;
    } else {
        columna -= 1;
    }
    if(inRange(numPaso, 1, 3)) {
        fila = 0
    }
    if(inRange(numPaso, 4, 6)) {
        fila = 1
    }
    if(inRange(numPaso, 7, 9)) {
        fila = 2
    }

    var v1 = m1[fila][0];
    var v2 = m1[fila][1];
    var v3 = m1[fila][2];
    var v4 = m2[0][columna];
    var v5 = m2[1][columna];
    var v6 = m2[2][columna];
    var span = document.createElement("SPAN");
    var span_content = document.createTextNode("(" + v1 + ") * (" + v4 + ") + (" + v2 + ") * (" + v5 + ") + (" + v3 + ") * (" + v6 + ")");
    span.appendChild(span_content);
    span.className = "math";
    var div = document.getElementById("div_info_mult");
    // Agrega un salto de línea antes del span
    var br = document.createElement('br');
    div.appendChild(br);
    div.appendChild(span);
}

/**
 * Función para colocar la matriz como tabla
 * @param {*} matrix La matriz la cual representar en tabla
 * @param {*} parent El padre de donde se va a colocar la tabla
 * @param {*} tableID El ID que va a llevar la tabla creada
 */
function drawMatrixAsTable(matrix, parent, tableID) {
    var matrixTable = matrix.getMatrix();
    var table = document.createElement('table');
    var tableBody = document.createElement('tbody');
    matrixTable.forEach(function(filaData) {
        var fila = document.createElement('tr');
        filaData.forEach(function(cellData) {
            var elemento = document.createElement('td');
            elemento.appendChild(document.createTextNode(cellData));
            fila.appendChild(elemento);
        });
        tableBody.appendChild(fila);
    });
    table.appendChild(tableBody);
    document.body.appendChild(table);
    // Agrega la clase para que se vea como una matriz
    table.classList.add('matrix');
    table.id = tableID;
    document.getElementById(parent.id).appendChild(table);
}

/**
 * Listener para que se ejecute cuando la ventana se cargue
 */
window.addEventListener("load", function(evt) {
    console.log("Main JS LOADED");
    // Matriz 01 de la sección Inicio 01
    var div_matriz_inicio_01 = document.getElementById("div_matriz_inicio_01");
    matrix = new Matrix("random");
    drawMatrixAsTable(matrix, div_matriz_inicio_01, "matriz_inicio_01");

    // Texto para la entrada de la matriz de la sección Inicio 01
    var m  = matrix.getMatrix();
    var filas = m.length;
    var columnas = m[0].length;
    indexFila = randomNumber(1, filas);
    indexColumna = randomNumber(1, columnas); 
    var input = document.getElementById("info_input_inicio");
    input.innerHTML = "¿Cuál es el valor de a"+ indexFila + indexColumna + "?";

    // Matriz 02 de la sección Inicio 02
    var div_matriz_inicio_02 = document.getElementById("div_matriz_inicio_02");
    var matrix2 = new Matrix("random");
    drawMatrixAsTable(matrix2, div_matriz_inicio_02, "matriz_inicio_02");
    // Matriz 03 de la sección Desarrollo, para la suma
    var div_matriz_desarrollo_suma_01 = document.getElementById("div_matriz_desarrollo_suma_01");
    matrix3 = new Matrix("3x3");
    drawMatrixAsTable(matrix3, div_matriz_desarrollo_suma_01, "matriz_desarrollo_suma_01");
    // Matriz 04 de la sección Desarrollo, para la suma
    var div_matriz_desarrollo_suma_02 = document.getElementById("div_matriz_desarrollo_suma_02");
    matrix4 = new Matrix("3x3");
    drawMatrixAsTable(matrix4, div_matriz_desarrollo_suma_02, "matriz_desarrollo_suma_02");
    // Escalar de la sección Desarrollo, para la multiplicación por escalar
    var div_matriz_desarrollo_escalar_01 = document.getElementById("div_matriz_desarrollo_escalar_01");
    escalar = new Number(randomNumber(-9, 9));
    var tableEscalar = document.getElementById("matriz_desarrollo_escalar_01");
    var fila = tableEscalar.insertRow(0);
    var celda = fila.insertCell(0);
    celda.innerHTML = escalar;
    // Matriz 05 de la sección Desarrollo, para la multiplicación por escalar
    var div_matriz_desarrollo_escalar_02 = document.getElementById("div_matriz_desarrollo_escalar_02");
    matrix5 = new Matrix("3x3");
    drawMatrixAsTable(matrix5, div_matriz_desarrollo_escalar_02, "matriz_desarrollo_escalar_02");
    // Matriz 06 de la sección Desarollo, para la multiplicacíón de matrices
    var div_matriz_desarrollo_mult_01 = document.getElementById("div_matriz_desarrollo_mult_01");
    matrix6 = new Matrix("3x3");
    drawMatrixAsTable(matrix6, div_matriz_desarrollo_mult_01, "matriz_desarrollo_mult_01");
    // Matriz 07 de la sección Desarollo, para la multiplicación de matrices
    var div_matriz_desarrollo_mult_02 = document.getElementById("div_matriz_desarrollo_mult_02");
    matrix7 = new Matrix("3x3");
    drawMatrixAsTable(matrix7, div_matriz_desarrollo_mult_02, "matriz_desarrollo_mult_02");
    // Matriz 08 del segundo ejercicio de Cierre
    var div_matriz_cierre_04 = document.getElementById("div_matriz_cierre_01");
    matrix8 = new Matrix("3x3");
    drawMatrixAsTable(matrix8, div_matriz_cierre_04, "matriz_cierre_01");
    // Matriz 09 del segundo ejercicio de Cierre
    var div_matriz_cierre_05 = document.getElementById("div_matriz_cierre_02");
    matrix9 = new Matrix("3x3");
    drawMatrixAsTable(matrix9, div_matriz_cierre_05, "matriz_cierre_02");
    // Matriz 10 del segundo ejercicio de Cierre
    var div_matriz_cierre_04 = document.getElementById("div_matriz_cierre_04");
    matrix10 = new Matrix("3x3");
    drawMatrixAsTable(matrix10, div_matriz_cierre_04, "matriz_cierre_04");
    // Matriz 11 del segundo ejercicio de Cierre
    var div_matriz_cierre_05 = document.getElementById("div_matriz_cierre_05");
    matrix11 = new Matrix("3x3");
    drawMatrixAsTable(matrix11, div_matriz_cierre_05, "matriz_cierre_05");
    randomCierre = randomNumber(1, 9);
    var nombre_matriz_cierre = document.getElementById("nombre_matriz_cierre_01");
    nombre_matriz_cierre.innerHTML = "A + " + randomCierre + "B =";
    var titulo_matriz_cierre = document.getElementById("titulo_op_cierre");
    titulo_matriz_cierre.innerHTML = "Calcular A + " + randomCierre + "B";

});

/**
 * Funcion que se ejecuta cuando se crea el DOM
 */
document.addEventListener("DOMContentLoaded", function() { 
    // Boton de siguiente de la sección Motivación
    document.querySelector('#siguiente_motivacion')
        .addEventListener('mousedown', function(event){    
            replaceElement("motivacion_01", "motivacion_02");

         });
    // Boton de anterior de la sección Motivación
    document.querySelector('#anterior_motivacion')
        .addEventListener('mousedown', function(event){
            replaceElement("motivacion_02", "motivacion_01");
    }); 
    //Boton de siguiente de la sección Inicio
    document.querySelector('#siguiente_inicio')
        .addEventListener('mousedown', function(event){
            replaceElement("inicio_01", "inicio_02");
     });   
    // Boton de anterior de la sección Inicio
    document.querySelector('#anterior_inicio')
        .addEventListener('mousedown', function(event){
            replaceElement("inicio_02", "inicio_01");  
    }); 
    // Boton de siguiente 01 de la sección Desarrollo
    document.querySelector('#siguiente_desarrollo_01')
        .addEventListener('mousedown', function(event){    
            replaceElement("desarrollo_01", "desarrollo_02");
     }); 
    // Boton de anterior 01 de la sección Desarrollo
    document.querySelector('#anterior_desarrollo_01')
        .addEventListener('mousedown', function(event){
            replaceElement("desarrollo_02", "desarrollo_01");
            
    });
    // Boton de siguiente 02 de la sección Desarrollo
    document.querySelector('#siguiente_desarrollo_02')
        .addEventListener('mousedown', function(event){    
            replaceElement("desarrollo_02", "desarrollo_03");
    }); 
    // Boton de anterior 02 de la sección Desarrollo
    document.querySelector('#anterior_desarrollo_02')
        .addEventListener('mousedown', function(event){
            replaceElement("desarrollo_03", "desarrollo_02");
    });
    // Boton de siguiente 01 de la sección Cierre
    document.querySelector('#siguiente_cierre_01')
        .addEventListener('mousedown', function(event){    
            //replaceElement("cierre_01", "cierre_02");
            replaceElement("cierre_01", "cierre_03");
    });
    // Boton de anterior 01 de la sección Cierre
    document.querySelector('#anterior_cierre_01')
        .addEventListener('mousedown', function(event){
            replaceElement("cierre_02", "cierre_01");
    });
    // Boton de siguiente 02 de la sección Cierre
    document.querySelector('#siguiente_cierre_02')
        .addEventListener('mousedown', function(event){
            replaceElement("cierre_02", "cierre_03");
    });
    // Boton de anterior 02 de la sección Cierre
    document.querySelector('#anterior_cierre_02')
        .addEventListener('mousedown', function(event){
            //replaceElement("cierre_03", "cierre_02");
            replaceElement("cierre_03", "cierre_01");
    });
    // Boton de siguiente 02 de la sección Cierre
    document.querySelector('#siguiente_cierre_03')
        .addEventListener('mousedown', function(event){    
            replaceElement("cierre_03", "cierre_04");
    });
    // Boton de anterior 02 de la sección Cierre
    document.querySelector('#anterior_cierre_03')
        .addEventListener('mousedown', function(event){
            replaceElement("cierre_04", "cierre_03");
    });

    // Boton de la matriz simétrica en la sección Inicio
    document.querySelector('#btn_simetrica')
        .addEventListener('mousedown', function(event){
            console.log("Botón matriz simética presionado");
            putMatrixType("simetrica");
    }); 
    // Boton de la matriz simétrica en la sección Inicio
    document.querySelector('#btn_superior')
        .addEventListener('mousedown', function(event){
            console.log("Boton matriz triangular superior presionado");
            putMatrixType("superior");
    }); 
    // Boton de la matriz simétrica en la sección Inicio
    document.querySelector('#btn_inferior')
        .addEventListener('mousedown', function(event){
            console.log("Botón matriz triangular inferior presionado");
            putMatrixType("inferior");
    }); 
    // Boton de la matriz simétrica en la sección Inicio
    document.querySelector('#btn_nula')
        .addEventListener('mousedown', function(event){
            console.log("Botón matriz nula presionado");
            putMatrixType("nula");
    }); 
    // Boton de la matriz simétrica en la sección Inicio
    document.querySelector('#btn_identidad')
        .addEventListener('mousedown', function(event){
            console.log("Botón matriz identidad presionado");
            putMatrixType("identidad");
    }); 
    // Boton de la matriz simétrica en la sección Inicio
    document.querySelector('#btn_diagonal')
        .addEventListener('mousedown', function(event){
            console.log("Botón matriz diagonal presionado");
            putMatrixType("diagonal");
    }); 

    //Boton que verifica la entrada introducida por el usuario en la sección
    // Inicio 01
    document.querySelector('#btn_input_inicio')
        .addEventListener('mousedown', function(event){
            var value = document.getElementById("input_inicio").value;
            value = parseInt(value);
            // Obtiene la matriz como un arreglo
            var m  = matrix.getMatrix();
            var mIndex = m[indexFila-1][indexColumna-1];
            //console.log(typeof value);
            //console.log(typeof mIndex);
            if(value === mIndex) {
                console.log("Correcto");
                alert("¡Muy bien! a"+ indexFila + indexColumna + " es igual a " + mIndex);
            } else {
                alert("¡Oh no! a"+ indexFila + indexColumna + " es igual a " + mIndex);
            }
            //console.log(value);
     }); 

    // Boton de paso 01 de la suma en la sección Desarrollo
    document.querySelector('#btn_paso_01_suma')
        .addEventListener('mousedown', function(event){
            console.log("Suma paso 01");
            var numPaso = 1;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_01_suma", "btn_paso_02_suma");
            // Muestra el S = A + B = 
            var nombre_matriz_suma = document.getElementById("nombre_matriz_suma");
            nombre_matriz_suma.style.display = "unset";
            // Encuentra y limpia el div donde va la suma de las matrices
            var div_resultado = document.getElementById("div_matriz_desarrollo_suma_03");
            div_resultado.innerHTML = "";
            // Agrega la matriz resultante
            var sumaMatrix = sumMatrix(matrix3, matrix4);
            drawMatrixAsTable(sumaMatrix, div_resultado, "matriz_desarrollo_suma_03");
            // Pone la tabla del color del fondo para volverla "invisible"
            document.getElementById("matriz_desarrollo_suma_03").style.color = "#FFF";
            // Cambia de color las filas de las matrices operandos
            changeRowOperator(numPaso, "matriz_desarrollo_suma_01", "matriz_desarrollo_suma_02");
            // Cambia el color de las fila de la matriz
            changeRowResult(numPaso, "matriz_desarrollo_suma_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_suma", "S1j es igual a la suma(término a término) de la primera fila de A con la primera fila de B:");
            // Muestra las operaciones en la columna derecha
            setOperandosSuma(numPaso);
    });
    // Boton de paso 02 de la suma en la sección Desarrollo
    document.querySelector('#btn_paso_02_suma')
        .addEventListener('mousedown', function(event){
            console.log("Suma paso 02");
            var numPaso = 2;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_02_suma", "btn_paso_03_suma");
            // Cambia de color las filas de las matrices operandos
            changeRowOperator(numPaso, "matriz_desarrollo_suma_01", "matriz_desarrollo_suma_02");
            // Cambia el color de la fila de la matriz
            changeRowResult(numPaso, "matriz_desarrollo_suma_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_suma", "S2j es igual a la suma(término a término) de la segunda fila de A con la segunda fila de B:");
            // Muestra las operaciones en la columna derecha
            setOperandosSuma(numPaso);
    });
    // Boton de paso 03 de la suma en la sección Desarrollo
    document.querySelector('#btn_paso_03_suma')
        .addEventListener('mousedown', function(event){
            console.log("Suma paso 03");
            var numPaso = 3;
            // Cambia de color las filas de las matrices operandos
            changeRowOperator(numPaso, "matriz_desarrollo_suma_01", "matriz_desarrollo_suma_02");
            // Cambia el color de la fila de la matriz
            changeRowResult(numPaso, "matriz_desarrollo_suma_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_suma", "S3j es igual a la suma(término a término) de la tercera fila de A con la tercera fila de B:");
            // Muestra las operaciones en la columna derecha
            setOperandosSuma(numPaso);
        
});
    // Boton de paso 01 de la multiplicación por escalar en la sección Desarrollo
    document.querySelector('#btn_paso_01_escalar')
        .addEventListener('mousedown', function(event){
            console.log("Escalar paso 01");
            var numPaso = 1;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_01_escalar", "btn_paso_02_escalar");
            // Muestra el S = A + B = 
            var nombre_matriz_suma = document.getElementById("nombre_matriz_escalar");
            nombre_matriz_suma.style.display = "unset";
            // Encuentra y limpia el div donde va la suma de las matrices
            var div_resultado = document.getElementById("div_matriz_desarrollo_escalar_03");
            div_resultado.innerHTML = "";
            // Agrega la matriz resultante
            var escalarMatrix = multEscalar(matrix5, escalar);
            drawMatrixAsTable(escalarMatrix, div_resultado, "matriz_desarrollo_escalar_03");
            // Pone la tabla del color del fondo para volverla "invisible"
            document.getElementById("matriz_desarrollo_escalar_03").style.color = "#FFF";
            // Cambia de color las filas de las matrices operandos
            changeTableOperatorEscalar(numPaso);
            var table_1 = document.getElementById("matriz_desarrollo_escalar_01");
            table_1.classList.add("fila_resultado");
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_escalar_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_escalar", "P11 es igual a la multiplicación de A11 por el escalar:");
            // Muestra las operaciones en la columna derecha
            setOperatorEscalar(numPaso);
    });
    // Boton de paso 02 de la multiplicación por escalar en la sección Desarrollo
    document.querySelector('#btn_paso_02_escalar')
        .addEventListener('mousedown', function(event){
            console.log("Escalar paso 02");
            var numPaso = 2;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_02_escalar", "btn_paso_03_escalar");
            // Cambia de color las filas de las matrices operandos
            changeTableOperatorEscalar(numPaso);
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_escalar_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_escalar", "P12 es igual a la multiplicación de A12 por el escalar:");
            // Muestra las operaciones en la columna derecha
            setOperatorEscalar(numPaso);
    });
    // Boton de paso 03 de la multiplicación por escalar en la sección Desarrollo
    document.querySelector('#btn_paso_03_escalar')
        .addEventListener('mousedown', function(event){
            console.log("Escalar paso 03");
            var numPaso = 3;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_03_escalar", "btn_paso_04_escalar");
            // Cambia de color las filas de las matrices operandos
            changeTableOperatorEscalar(numPaso);
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_escalar_03"); 
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_escalar", "P13 es igual a la multiplicación de A13 por el escalar:");           
            // Muestra las operaciones en la columna derecha
            setOperatorEscalar(numPaso);
    });
    // Boton de paso 04 de la multiplicación por escalar en la sección Desarrollo
    document.querySelector('#btn_paso_04_escalar')
        .addEventListener('mousedown', function(event){
            console.log("Escalar paso 04");
            var numPaso = 4;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_04_escalar", "btn_paso_05_escalar");
            // Cambia de color las filas de las matrices operandos
            changeTableOperatorEscalar(numPaso);
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_escalar_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_escalar", "P21 es igual a la multiplicación de A21 por el escalar:");
            // Muestra las operaciones en la columna derecha
            setOperatorEscalar(numPaso);
    });
    // Boton de paso 05 de la multiplicación por escalar en la sección Desarrollo
    document.querySelector('#btn_paso_05_escalar')
        .addEventListener('mousedown', function(event){
            console.log("Escalar paso 05");
            var numPaso = 5;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_05_escalar", "btn_paso_06_escalar");
            // Cambia de color las filas de las matrices operandos
            changeTableOperatorEscalar(numPaso);
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_escalar_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_escalar", "P22 es igual a la multiplicación de A22 por el escalar:");
            // Muestra las operaciones en la columna derecha
            setOperatorEscalar(numPaso);
    });
    // Boton de paso 06 de la multiplicación por escalar en la sección Desarrollo
    document.querySelector('#btn_paso_06_escalar')
        .addEventListener('mousedown', function(event){
            console.log("Escalar paso 06");
            var numPaso = 6;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_06_escalar", "btn_paso_07_escalar");
            // Cambia de color las filas de las matrices operandos
            changeTableOperatorEscalar(numPaso);
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_escalar_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_escalar", "P23 es igual a la multiplicación de A23 por el escalar:");
            // Muestra las operaciones en la columna derecha
            setOperatorEscalar(numPaso);
    });
    // Boton de paso 07 de la multiplicación por escalar en la sección Desarrollo
    document.querySelector('#btn_paso_07_escalar')
        .addEventListener('mousedown', function(event){
            console.log("Escalar paso 07");
            var numPaso = 7;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_07_escalar", "btn_paso_08_escalar");
            // Cambia de color las filas de las matrices operandos
            changeTableOperatorEscalar(numPaso);
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_escalar_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_escalar", "P31 es igual a la multiplicación de A32 por el escalar:");
            // Muestra las operaciones en la columna derecha
            setOperatorEscalar(numPaso);
    });
    // Boton de paso 08 de la multiplicación por escalar en la sección Desarrollo
    document.querySelector('#btn_paso_08_escalar')
        .addEventListener('mousedown', function(event){
            console.log("Escalar paso 08");
            var numPaso = 8;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_08_escalar", "btn_paso_09_escalar");
            // Cambia de color las filas de las matrices operandos
            changeTableOperatorEscalar(numPaso);
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_escalar_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_escalar", "P32 es igual a la multiplicación de A32 por el escalar:");
            // Muestra las operaciones en la columna derecha
            setOperatorEscalar(numPaso);
    });
    // Boton de paso 09 de la multiplicación por escalar en la sección Desarrollo
    document.querySelector('#btn_paso_09_escalar')
        .addEventListener('mousedown', function(event){
            console.log("Escalar paso 09");
            var numPaso = 9;
            // Cambia de color las filas de las matrices operandos
            changeTableOperatorEscalar(numPaso);
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_escalar_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_escalar", "P3 es igual a la multiplicación de A33 por el escalar:");
            // Muestra las operaciones en la columna derecha
            setOperatorEscalar(numPaso);
    });
    // Boton de paso 01 de la multiplicación de matrices en la sección de Desarrollo
    document.querySelector('#btn_paso_01_mult')
        .addEventListener('mousedown', function(event){
            console.log("Multiplicación paso 01");
            var numPaso = 1;          
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_01_mult", "btn_paso_02_mult");
            // Muestra el R = A * B = 
            var nombre_matriz_suma = document.getElementById("nombre_matriz_mult");
            nombre_matriz_suma.style.display = "unset";
            // Encuentra y limpia el div donde va la suma de las matrices
            var div_resultado = document.getElementById("div_matriz_desarrollo_mult_03");
            div_resultado.innerHTML = "";
            // Agrega la matriz resultante
            var multMatrix = sumMatrix(matrix6, matrix7);
            drawMatrixAsTable(multMatrix, div_resultado, "matriz_desarrollo_mult_03");
            // Pone la tabla del color del fondo para volverla "invisible"
            document.getElementById("matriz_desarrollo_mult_03").style.color = "#FFF";
            // Cambia las filas de la matriz 01
            changeRowOperator(numPaso, "matriz_desarrollo_mult_01", "matriz_desarrollo_mult_01");
            // Cambia las columnas de la matriz 02
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_mult_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_mult", "R11 es igual a la suma de los productos (término a término) de la primera fila de A por la primera columna de B:");
            // Muestra las operaciones en la columna derecha
            setOperatorMult(numPaso); 
    }); 
    // Boton de paso 02 de la multiplicación de matrices en la sección de Desarrollo
    document.querySelector('#btn_paso_02_mult')
        .addEventListener('mousedown', function(event){
            console.log("Multiplicación paso 02");
            var numPaso = 2;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_02_mult", "btn_paso_03_mult");
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_mult_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_mult", "R12 es igual a la suma de los productos (término a término) de la primera fila de A por la segunda columna de B:");
            // Muestra las operaciones en la columna derecha
            setOperatorMult(numPaso); 
    }); 
    // Boton de paso 03 de la multiplicación de matrices en la sección de Desarrollo
    document.querySelector('#btn_paso_03_mult')
        .addEventListener('mousedown', function(event){
            console.log("Multiplicación paso 03");
            var numPaso = 3;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_03_mult", "btn_paso_04_mult");
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_mult_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_mult", "R13 es igual a la suma de los productos (término a término) de la primera fila de A por la tercera columna de B:");
            // Muestra las operaciones en la columna derecha
            setOperatorMult(numPaso); 
    });
    // Boton de paso 04 de la multiplicación de matrices en la sección de Desarrollo
    document.querySelector('#btn_paso_04_mult')
        .addEventListener('mousedown', function(event){
            console.log("Multiplicación paso 04");
            var numPaso = 4;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_04_mult", "btn_paso_05_mult");
            // Cambia las filas de la matriz 01
            changeRowOperator(2, "matriz_desarrollo_mult_01", "matriz_desarrollo_mult_01");
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_mult_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_mult", "R21 es igual a la suma de los productos (término a término) de la segunda fila de A por la primera columna de B:");
            // Muestra las operaciones en la columna derecha
            setOperatorMult(numPaso); 
    });
    // Boton de paso 05 de la multiplicación de matrices en la sección de Desarrollo
    document.querySelector('#btn_paso_05_mult')
        .addEventListener('mousedown', function(event){
            console.log("Multiplicación paso 05");
            var numPaso = 5;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_05_mult", "btn_paso_06_mult");
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_mult_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_mult", "R22 es igual a la suma de los productos (término a término) de la segunda fila de A por la segunda columna de B:");
            // Muestra las operaciones en la columna derecha
            setOperatorMult(numPaso); 
    });
    // Boton de paso 06 de la multiplicación de matrices en la sección de Desarrollo
    document.querySelector('#btn_paso_06_mult')
        .addEventListener('mousedown', function(event){
            console.log("Multiplicación paso 06");
            var numPaso = 6;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_06_mult", "btn_paso_07_mult");
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_mult_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_mult", "R23 es igual a la suma de los productos (término a término) de la segunda fila de A por la tercera columna de B:");
            // Muestra las operaciones en la columna derecha
            setOperatorMult(numPaso); 
    });
    // Boton de paso 07 de la multiplicación de matrices en la sección de Desarrollo
    document.querySelector('#btn_paso_07_mult')
        .addEventListener('mousedown', function(event){
            console.log("Multiplicación paso 07");
            var numPaso = 7;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_07_mult", "btn_paso_08_mult");
            // Cambia las filas de la matriz 01
            changeRowOperator(3, "matriz_desarrollo_mult_01", "matriz_desarrollo_mult_01");
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_mult_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_mult", "R31 es igual a la suma de los productos (término a término) de la tercera fila de A por la primera columna de B:");
            // Muestra las operaciones en la columna derecha
            setOperatorMult(numPaso); 
    });
    // Boton de paso 08 de la multiplicación de matrices en la sección de Desarrollo
    document.querySelector('#btn_paso_08_mult')
        .addEventListener('mousedown', function(event){
            console.log("Multiplicación paso 08");
            var numPaso = 8;
            // Sustituye el botón por que el del siguiente paso
            replaceElement("btn_paso_08_mult", "btn_paso_09_mult");
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_mult_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_mult", "R32 es igual a la suma de los productos (término a término) de la tercera fila de A por la segunda columna de B:");
            // Muestra las operaciones en la columna derecha
            setOperatorMult(numPaso); 
    });
    // Boton de paso 09 de la multiplicación de matrices en la sección de Desarrollo
    document.querySelector('#btn_paso_09_mult')
        .addEventListener('mousedown', function(event){
            console.log("Multiplicación paso 09");
            var numPaso = 9;
            // Cambia el color de las fila de la matriz resultado
            changeTableMultEscalar(numPaso, "matriz_desarrollo_mult_03");
            // Cambia el texto de la columna derecha
            changeTextInfoDesarrollo("div_info_mult", "R33 es igual a la suma de los productos (término a término) de la tercera fila de A por la tercera columna de B:");
            // Muestra las operaciones en la columna derecha
            setOperatorMult(numPaso); 
    });
    // Boton de paso 09 de la multiplicación de matrices en la sección de Desarrollo
    document.querySelector('#btn_verificar_02')
        .addEventListener('mousedown', function(event){
            console.log("Verificar 03");            // Cambia el color de las fila de la matriz resultado
            // Muestra el S = A + B = 
            var nombre_matriz_suma = document.getElementById("nombre_matriz_cierre_01");
            nombre_matriz_suma.style.display = "unset";
            // Encuentra y limpia el div donde va la suma de las matrices
            var div_resultado = document.getElementById("div_matriz_cierre_03");
            div_resultado.innerHTML = "";
            // Agrega la matriz resultante
            var escalarMatrix = multEscalar(matrix11, randomCierre);
            var summMatrix = sumMatrix(matrix10, escalarMatrix);
            drawMatrixAsTable(summMatrix, div_resultado, "matriz_desarrollo_cierre_03");
    });
    // Boton de paso 09 de la multiplicación de matrices en la sección de Desarrollo
    document.querySelector('#btn_verificar_03')
        .addEventListener('mousedown', function(event){
            console.log("Verificar 03");            // Cambia el color de las fila de la matriz resultado
            // Muestra el S = A + B = 
            var nombre_matriz_suma = document.getElementById("nombre_matriz_cierre_02");
            nombre_matriz_suma.style.display = "unset";
            // Encuentra y limpia el div donde va la suma de las matrices
            var div_resultado = document.getElementById("div_matriz_cierre_06");
            div_resultado.innerHTML = "";
            // Agrega la matriz resultante
            var multMatrix = sumMatrix(matrix10, matrix11);
            drawMatrixAsTable(multMatrix, div_resultado, "matriz_desarrollo_cierre_06");
    });
});


         