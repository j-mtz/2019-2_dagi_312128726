export default class Matrix{
    /**
     * Constructor único de la clase para representar matrices.
     * Como JS no acepta múltiples constructores, los definimos con una variable
     * @param {*} tipo 
     */
    constructor(tipo) {
        // Caso especial para construir una Matriz a partir de un arreglo
        if(tipo instanceof Array){
            this.matrix = tipo;
            console.log("Matriz creada a partir de arreglo");
        }
        // Caso especial para construir una Matriz con un solo elemento
        if(tipo instanceof Number){
            this.matrix = [tipo];
            console.log("Matriz de un elemento");
        }

        switch(tipo) {
            case "random":
                this.matrix = this.matrixRandom();
                console.log("Matriz aleatoria");
                break;
            case "simetrica":
                this.matrix = this.matrixSimetric();
                console.log("Matriz simétrica");
                break;
            case "superior":
                this.matrix = this.matrixTriangularSuperior();
                console.log("Matriz triangular superior");
                break;
            case "inferior":
                this.matrix = this.matrixTriangularInferior();
                console.log("Matriz triangular inferior");
                break;
            case "nula":
                this.matrix = this.matrixNullFija();
                console.log("Matriz nula");
                break;
            case "identidad":
                this.matrix = this.matrixIdentity();
                console.log("Matriz identidad")
                break;
            case "diagonal":
                this.matrix = this.matrixDiagonal();
                console.log("Matriz diagonal");
                break;
            case "3x3":
                this.matrix = this.generateMatrix3();
                console.log("Matriz 3x3");
                break;
        }
        //this.printMatrix();
    }

    /**
     * Método para obtener el arreglo que representa a la matriz
     */
    getMatrix(){
        return this.matrix;
    }

    /**
     * Método para obtener la longitud de una matriz
     * La longitud es el número de filas.
     * Trabaja bien en casi todos los casoa ya que la mayoría son matrices
     * cuadradas.
     */
    getLength(){
        return this.matrix.length;
    }

    /**
     * Método para gener un número aleatorio dentro del rango dado
     * @param {*} min Rango mínimo
     * @param {*} max Rango máximo
     */
    randomNumber(min, max) {
        return Math.floor(Math.random()*(max-min+1)+min);
    }

    /**
     * Método para crear una matriz nula
     * Crea una matriz cuadrada nula del tamaño dado.
     * Una matriz nula tiene todas sus entradas en 0's.
     */
    matrixNull(size) {
        var nula = new Array(size);
        for(var i = 0; i < size; i++) {
            for(var j = 0; j < size; j++) {
                nula[i] = Array(size).fill(0);
            }
        }
        return nula;
    }

    matrixRandom() {
        var filas = this.randomNumber(2, 8);
        var columnas = this.randomNumber(2, 8);
        var random = new Array(filas);
        for(var i = 0; i < filas; i++) {
            random[i] = new Array(columnas);
            for(var j = 0; j < columnas; j++) {
                random[i][j] = this.randomNumber(-9, 9);
            }
        }
        return random;
    }

    /**
     * Método para crear una matriz simétrica.
     * Para cada valor se cumple que aij = aji.
     */
    matrixSimetric(){
        // Primero obtiene una matriz triangular
        var simetrica = this.matrixTriangularInferior();
        var size = simetrica.length;
        for(var i = 0; i < size ; i++) {
            for(var j = 0; j < size; j++) {
                // Recorre la matriz y coloca su valor inverso
                simetrica[i][j] = simetrica[j][i];
            }
        }
        return simetrica;
    }

    /**
     * Método para crear una matriz diagonal superior.
     * Con valores mayores iguales a 0 por debajo de la diagonal.
     */
    matrixTriangularSuperior() {
        // Primero obtiene una matriz diagonal
        var triangular = this.matrixDiagonal();
        var size = triangular.length;
        for(var i = 0; i < size; i++) {
            for(var j = 0; j < size; j++) {
                // Cambia los valores superiores de la diagonal
                if(i < j){
                    triangular[i][j] = this.randomNumber(-9, 9);
                }
            }
        }
        return triangular;
    }

    /**
     * Método para crear una matriz diagonal inferior.
     * Con valores mayores iguales a 0 por debajo de la diagonal.
     */
    matrixTriangularInferior() {
        // Primero obtiene una matriz diagonal
        var triangular = this.matrixDiagonal();
        var size = triangular.length;
        for(var i = 0; i < size; i++) {
            for(var j = 0; j < size; j++) {
                // Cambia los valores debajo de la diagonal
                if(i > j){
                    triangular[i][j] = this.randomNumber(-9, 9);
                }
            }
        }
        return triangular;
    }

    /**
     * Método para crear una matriz nula.
     * Una matriz nula tiene todas sus entradas en 0's.
     */
    matrixNullFija() {
        var size = this.randomNumber(2, 8);
        var nula = this.matrixNull(size);
        return nula;
    }

    /**
     * Método para crear una matriz identidad.
     * 1's en la diagonal principal y el resto es 0.
     */
    matrixIdentity() {
        var size = this.randomNumber(2, 8);
        // Primero crea una matriz nula
        var identidad = this.matrixNull(size);
        // Después coloca la identidad
        for(var i = 0; i < size; i++) {
            for(var j = 0; j < size; j++) {
                if(i === j){
                    identidad[i][j] = 1;
                }
            }
        }
        return identidad;
    }

    /**
     * Método para crear una matriz diagonal.
     * Los elementos fuera de la diagonal principal son todos iguales a cero,
     * debe de haber al menos un elemento diferente de cero en la diagonal 
     * principal.
     */
    matrixDiagonal() {
        var size = this.randomNumber(2, 8);
        // Primero crea una matriz nula
        var diagonal = this.matrixNull(size);
        // Después los elementos de la diagonal
        for(var i = 0; i < size; i++) {
            for(var j = 0; j < size; j++) {
                if(i === j){
                    diagonal[i][j] = this.randomNumber(-9, 9);
                }
            }
        }
        return diagonal;
    }

    /**
     * Método para generar una matriz aleatoria cuadrada de 3 x 3.
     *  Usada para mostrar la suma y multiplicación.
     */
    generateMatrix3(){
        var size = 3;
        var m = this.matrixNull(size);
        for(var i = 0; i < size ; i++){
            for(var j = 0; j < size; j++) {
                m[i][j] = this.randomNumber(-9, 9);
            }
        }
        return m;
    }

    /**
     * Método para multiplicar la matriz con otra matriz que recibe como parámetro.
     * @param {*} otherMatrix La matriz a multiplicar con la actual.
     */
    multMatrix(otherMatrix){
        var m1 = this.matrix;
        var m2 = otherMatrix.getMatrix();
        var size = m1.length;
        var result = [];
        for(var i = 0; i < size; i++){
            result[i] = [];
            for(var j = 0; j < size; j++){
                var sum = 0;
                for(var k = 0; k < size; k++){
                    sum += m1[i][k] * m2[k][j];
                }
                result[i][j] = sum;
            }
        }
        return new Matrix(result);
    }

    /**
     * Método para imprimir en la consola una matriz de una forma agradable
     * para el desarrollador.
     */
    printMatrix() {
        var matrixPrint = '';
        for(var i = 0; i < this.matrix.length; i++) {
            for(var j = 0; j < this.matrix[i].length; j++) {
                matrixPrint += this.matrix[i][j];
                if(this.matrix[i][j] > 9) {
                    matrixPrint += " ";
                } else {
                    matrixPrint += "  ";
                }
            }
            console.log(matrixPrint);
            matrixPrint = '';
        }
        console.log("\n");
    }
}