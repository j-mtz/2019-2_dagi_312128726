import KEY from "./Key.js";

// Una celda es un 0 si está vacia
const EMPTY_CELL = '0';
// Una celda es un 1 si tiene una serpiente
const SNAKE_BODY = '1';
// Una celda es un 2 si tiene comida
const FOOD = '2';

const GRID_WIDTH = 64;
const GRID_HEIGTH = 48;

export default class Interactive {
  constructor() {
    this.svg = document.querySelector("svg");
    this.svg_namespace = "http://www.w3.org/2000/svg";

    // Informacion del puntaje
    this.score = 0;
    this.scoreInfo = document.getElementById("score");
    // Direccion hacia donde se mueve la serpiente
    this.direction = KEY.UP;
    // Posicion inicial de la cabeza
    // La cabeza es el primer elemento que se mueve,
    // por eso es necesario separar este elemento del cuerpo.
    this.snakeHead = '1503';
    // Arreglo que representa la serpiente y sus posiciones
    this.snakeArray = [this.snakeHead];
    // Arreglo que representa el 'back-end' de la cuadrícula
    // se llena con 0 para representar todas las casillas vacías
    this.grid = Array.from(EMPTY_CELL.repeat(GRID_WIDTH * GRID_HEIGTH));
    // Posicion inicial de la serpiente
    this.grid[this.snakeHead] = SNAKE_BODY;
    // Crea la cuadricula 
    this.createBoard(); 
    // Crea la primera casilla de comida
    this.food = this.generateFood();
    // Variable para saber si el juego sigue
    this.game = true;
    // Cuadro de fin de juego
    this.retro = document.querySelector("#retro");

    window.addEventListener("keydown", (evt) => {
      KEY.onKeyDown(evt.key);
    });

    window.addEventListener("keyup", (evt) => {
      KEY.onKeyUp(evt.key);
    });

    document.querySelector("#retro_btn")
    .addEventListener("mousedown", () => window.location.reload() );
  }

  /**
   * Método para procesar la entrada para el juego.
   * Cambia la dirección hacia donde se mueve la serpiente.
   */
  processInput() {
    if (KEY.isPressed(KEY.LEFT)) {
      this.direction = KEY.LEFT;
    }
    if (KEY.isPressed(KEY.RIGHT)) {
      this.direction = KEY.RIGHT;
    }
    if (KEY.isPressed(KEY.UP)) {
      this.direction = KEY.UP;
    }
    if (KEY.isPressed(KEY.DOWN)) {
      this.direction = KEY.DOWN;
    }
  }

  /**
   * Método para actualizar la lógica del juego.
   * Cambia la cabeza con base en la dirección y revisa el estado 
   * del juego.
   * @param {} elapsed 
   */
  update(elapsed) {
    this.updateSnakeHead();
    this.checkSnakeDead();
    this.updateSnakeBody(this.snakeHead);
    this.checkFood();
  }

  /**
   * Método para renderizar el juego.
   */
  render() {
    if(this.game){
      this.refreshGrid();
    }
  }

  /**
   * Crea la cuadrícula de juego.
   */
  createBoard(){
    for(var i = 0; i < 64; i++){
      for(var j = 0; j < 48; j++){
        let rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        rect.setAttribute("x", i*12);
        rect.setAttribute("y", j*12);
        rect.setAttribute("width", 12);
        rect.setAttribute("height", 12);
        rect.setAttribute("fill", "#0F380F");
        rect.setAttribute("stroke", "#306230");
        rect.setAttribute("stroke-width", 2);
        this.svg.appendChild(rect);
      }
    }
  }

  /**
   * Método para generar una posición de comida aleatoriamente
   */
  generateFood() {
    var random = Math.floor((Math.random() * 3071) + 1);
    // Si la casilla donde se pretende colocar es distinta de 0,
    // significa que es una casilla ocupada
    if(this.grid[random] != '0') {
      generateFood();
    } else {
      this.grid[random] = FOOD;
    }
    console.log("COMIDA: ", random);
    return random;
  }

  /**
   * Método para actualizar la dirección y la cabeza de la serpiente.
   */
  updateSnakeHead() {
    switch (this.direction) {
      case KEY.LEFT:
        //console.log("LEFT");
        this.snakeHead--;
        break;
      case KEY.RIGHT:
        //console.log("RIGHT");
        this.snakeHead++;
        break;
      case KEY.UP:
        //console.log("UP");
        this.snakeHead -= GRID_WIDTH;
        break;
      case KEY.DOWN:
        //console.log("DOWN");
        // HotFix: Al sumar lo interpreta como una cadena y no realiza
        // la operacion correctamente
        var tempCast = parseInt(this.snakeHead);
        tempCast += GRID_WIDTH;
        this.snakeHead = tempCast;
        break;
    }
  }

  /**
   * Método que revisa si la serpiente sigue en juego.
   */
  checkSnakeDead() {
    // Caso cuando la serpiente se sale del borde superior o ingerior
    if(this.snakeHead < 0 || this.snakeHead > GRID_HEIGTH * GRID_WIDTH) {
      this.game = false;
      this.retro.style.display = "block";
    }
    // Caso cuando la cabeza de la serpiente toca su cuerpo
    for(var i = 1; i < this.snakeArray.length; i++){
      if(this.snakeHead == this.snakeArray[i]){
        this.game = false;
        this.retro.style.display = "block";      }
    }
    // Caso cuando choca del borde derecho e izquierdo
    switch (this.direction) {
      // Lado izquierdo
      case KEY.LEFT:
          if ((this.snakeHead % 64) === 63) {
            this.game = false;
            this.retro.style.display = "block";
          }
          break;
      // Lado derecho
      case KEY.RIGHT:
          if ((this.snakeHead % 64) === 0) {
            this.game = false;
            this.retro.style.display = "block";
          }
          break;
    }
  }

  /**
   * Método que actualiza el cuerpo de la serpiente.
   * Agrega el valor de la nueva cabeza y elimina la casilla anterior
   * @param {} newHead La posicion de la nueva cabeza
   */
  updateSnakeBody(newHead) {
    // Agrega la nueva cabeza al arreglo y le indica el color a la cuadricula
    this.snakeArray.unshift(newHead);
    this.grid[this.snakeArray[0]] = SNAKE_BODY;
    // Saca el ultimo elemento y regresa la casilla a vacia
    var tailSnake = this.snakeArray.pop();
    this.grid[tailSnake] = EMPTY_CELL; 
  }

  /**
   * Método que revisa si se está en una casilla de comida.
   * Aumenta el score y agrega una casilla mas a la serpiente.
   */
  checkFood() {
    if(this.snakeHead == this.food) {
      this.score++;
      this.scoreInfo.innerText = "Score: " + this.score;
      this.snakeArray.unshift(this.snakeHead);
      this.food = this.generateFood();
    }
  }

  /**
   * Método para dibujar la cuadrícula tras cada ciclo de renderizado.
   * Usa las constantes y las posiciones para saber que dibujar.
   */
  refreshGrid() {
    var k = 0;
    for(var i = 0; i < 48; i++){
      for(var j = 0; j < 64; j++){
        var valueGrid = this.grid[k];
        let rect = document.createElementNS(this.svg_namespace, "rect");
        switch(valueGrid){
          case EMPTY_CELL:
            rect.setAttribute("fill", "#0F380F");
            rect.setAttribute("stroke", "#306230");
            break;
          case SNAKE_BODY:
            rect.setAttribute("fill", "#9bbc0f");
            rect.setAttribute("stroke", "#9bbc0f");
            break;
          case FOOD:
            rect.setAttribute("fill", "#e74c3c");
            rect.setAttribute("stroke", "#e74c3c");
            break;
          // Si no es ninguna de las anteriores, es un error 
          // o lee un valor inválido y termina el juego
          default:
            this.retro.style.display = "block";
            this.game = false;
            console.log("this game ", this.game);
            break;
        }
        rect.setAttribute("x", j*12);
        rect.setAttribute("y", i*12);
        rect.setAttribute("width", 12);
        rect.setAttribute("height", 12);
        rect.setAttribute("stroke-width", 2);
        this.svg.appendChild(rect);
        k++;
      }
    }
  }
}
