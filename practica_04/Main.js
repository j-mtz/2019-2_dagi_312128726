import Interactive from "./Interactive.js";

window.addEventListener("load", function(evt) {
  let lastTime = Date.now();
  let current = 0;
  let elapsed = 0;
  let max_elapsed_wait = 30/1000;

  let interactive = new Interactive();

  (function gameLoop() {
    current = Date.now();
    elapsed = (current - lastTime) / 1000;

    if (elapsed > max_elapsed_wait) {
      elapsed = max_elapsed_wait;
    }

    interactive.processInput();
    interactive.update(elapsed);
    interactive.render();

    //sleep(100);

    lastTime = current;

    window.requestAnimationFrame(gameLoop);
  })();
});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}