export default class Controller {
  constructor() { }

  init(model, view) {
    this.model = model;
    this.view = view;
  }

  open() {
    let file_input = document.createElement("input");
    file_input.setAttribute("type", "file");
    file_input.setAttribute("accept", "image/png,image/jpeg");

    file_input.addEventListener("change", (evt) => {
      let file = file_input.files[0];
      
      const reader = new FileReader();
      reader.addEventListener("load", (elem) => {
        let image = document.createElement("img");
        
        image.addEventListener("load", () => {
          this.view.setTitle(file.name);
          this.view.changeSize(image.width, image.height);
          this.model.setImage(image);
        });
        image.src = reader.result;
      });
      if (file) {
        reader.readAsDataURL(file);
      }
    });

    file_input.click();
  }

  save() {
    let link = document.createElement("a");
    link.setAttribute("download", "image.png");
    document.body.appendChild(link);
    
    link.addEventListener("click", (evt) => {
      link.href = this.model.getImage();
      document.body.removeChild(link);
    });

    link.click();
  }

  undo() {
    this.model.undo();
  }

  /**
   * Método del controlador para volver a hacer la acción anterior.
   */
  redo() {
    this.model.redo();
  }

  setStrokeColor(color) {
    this.strokeColor = color;
  }

  /**
   * Método del controlador para modificar el color del relleno
   * @param {*} color El color de relleno
   */
  setFillColor(color) {
    this.fillColor = color;
  }

  line_mode() {
    this.mode = "line";
  }

  free_mode() {
    this.mode = "free";
  }

  /**
   * Método del controlador para poner el programa
   * en modo de dibujo de rectángulo
   */
  rect_mode() {
    this.mode = "rect";
  }

  /**
   * Método del controlador para poner el programa
   * en modo de dibujo de rectángulo con relleno
   */
  rect_fill_mode() {
    this.mode = "rect_fill";
  }

  /**
   * Método del controlador para poner el programa
   * en modo de dibujo de círculo
   */
  circ_mode() {
    this.mode = "circ";
  }

  /**
   * Método del controlador para poner el programa
   * en modo de dibujo de círculo con relleno
   */
  circ_fill_mode() {
    this.mode = "circ_fill";
  }

  mouse_down(evt, canvas) {
    // Se agregan los modos para los lineas
    if ((this.mode === "line") || (this.mode === "free") ||
        (this.mode === "rect") || (this.mode === "rect_fill") ||
        (this.mode === "circ") || (this.mode === "circ_fill")) {
      this.rect = canvas.getBoundingClientRect();
      this.init_x = evt.clientX - this.rect.left;
      this.init_y = evt.clientY - this.rect.top;
      // Ademas del color de la linea se agrega el color de fondo
      this.view.setStrokeColor(this.strokeColor);
      this.view.setFillColor(this.fillColor);
    }

    if (this.mode === "free") {
      this.view.initFree(this.init_x, this.init_y);
    }
  }

  mouse_move(evt) {
    if (this.mode === "line") {
      this.view.drawLine(
        this.init_x, this.init_y, 
        evt.clientX - this.rect.left, evt.clientY - this.rect.top
      );
    } else if (this.mode === "free") {
      this.view.continueFree(evt.clientX - this.rect.left, evt.clientY - this.rect.top);
    // Se aggregan los demas eventos para cuando el mouse se mueve y el modo está seleccionado
    } else if (this.mode === "rect") {
    this.view.drawRect(this.init_x, this.init_y, 
      evt.clientX - this.rect.left, evt.clientY - this.rect.top);
    } else if (this.mode === "rect_fill") {
    this.view.drawRectFill(this.init_x, this.init_y,
      evt.clientX - this.rect.left, evt.clientY - this.rect.top);
    } else if (this.mode === "circ") {
    this.view.drawCirc(this.init_x, this.init_y,
      evt.clientX - this.rect.left, evt.clientY - this.rect.top);
    } else if (this.mode === "circ_fill") {
    this.view.drawCircFill(this.init_x, this.init_y,
      evt.clientX - this.rect.left, evt.clientY - this.rect.top);
    }
}

  mouse_up(aux_canvas) {
    this.model.putImage(aux_canvas);

    this.view.clear();
  }
}